#!/usr/bin/python

import sys
import csv
from datetime import datetime

def mapper():
    reader = csv.reader(sys.stdin, delimiter='\t')
    for line in reader:
        try:
            student_id = line[3]
            date_field = line[8].strip()
            date = date_field[:-3]
            hour = datetime.strptime(date[:-3], '%Y-%m-%d %H:%M:%S.%f').hour
            print student_id, '\t', hour

        except:
            continue

if __name__ == '__main__':
    mapper()
