#!/usr/bin/python

import sys


def reducer():
    hours = [0]*24
    user = None
    for line in sys.stdin:
        data = line.strip().split('\t')
        if len(data) != 2:
            continue

        current_user, current_hour =  data



        if user and current_user != user:
            print_user(user,hours, max(hours))
            hours = [0] * 24

        user=current_user
        hours[int(current_hour)] += 1



    print_user(user, hours, max(hours))


def print_user(user_id, hours, max_value):
    for hour, value in enumerate(hours):
        if value == max_value:
            print user_id, '\t', hour


if __name__ == '__main__':
    reducer()
