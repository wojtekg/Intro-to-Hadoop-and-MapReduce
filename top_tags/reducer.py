#!/usr/bin/python

import sys


def reducer():
    cnt = dict()
    for line in sys.stdin:
        data = line.strip().split('\t')

        for tag in data:
            if tag not in cnt.keys():
                cnt[tag] = 0
            cnt[tag] += 1

    for tag in sorted(cnt, key=cnt.get, reverse=True)[:10]:
        print tag, '\t', cnt[tag]


if __name__ == '__main__':
  reducer()
