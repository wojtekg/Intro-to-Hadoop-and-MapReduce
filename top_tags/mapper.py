#!/usr/bin/python

import sys
import csv


def mapper():
    reader = csv.reader(sys.stdin, delimiter='\t')
    for line in reader:
        try:
            tagnames = line[2].split(' ')
            node_type = line[5]
            if node_type == 'question':
                print '\n'.join(tagnames)
        except:
            continue


if __name__ == '__main__':
  mapper()
