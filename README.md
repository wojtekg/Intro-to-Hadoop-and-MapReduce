# Intro-to-Hadoop-and-MapReduce
Final Project for Intro to Hadoop and MapReduce provided by Udacity

student_test_posts.csv is a sample file for which MapReduce code run.
Instead of running jobs on Hadoop, you can "simulate" it by running on Linux 
command line:

```shell
cat ..student_test_posts.csv | ./mapper.py | sort | ./reducer.py
```

## Tasks:
* student_times - finding what hour the student mostly post.

* post_answer_length - average length of answer for a post.

* top_tags - 10 top tags, ordered by the number of questions they appear in.

* study_groups - list groups of people who communicate between themselves.
