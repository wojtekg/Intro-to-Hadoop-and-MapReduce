#!/usr/bin/python

import sys


def reducer():
    question_id = None
    authors = []

    for line in sys.stdin:
        data = line.strip().split('\t')
        if len(data) != 2:
            continue

        current_question_id, author_id = data

        if question_id and question_id != current_question_id:
            print question_id, '\t', authors
            authors = []

        question_id = current_question_id
        authors.append(author_id)
    print question_id, '\t', authors


if __name__ == '__main__':
    reducer()
