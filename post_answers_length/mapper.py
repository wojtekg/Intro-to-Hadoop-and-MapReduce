#!/usr/bin/python

import sys
import csv

def mapper():
    reader = csv.reader(sys.stdin, delimiter='\t')
    for line in reader:
        try:
            post_id = int(line[0])
            parent_id = line[6]
            body_length = len(line[4].strip())
            node_type = line[5]
            if node_type == 'answer':
                mixed_id = parent_id
            else:
                mixed_id = post_id

            print mixed_id,'\t', body_length, '\t', node_type

        except:
            continue


if __name__ == '__main__':
  mapper()
