#!/usr/bin/python

import sys

def reducer():
    text_id = None
    answer_sum_length = 0
    answer_count = 0
    question_length = 0


    for line in sys.stdin:
        data = line.strip().split('\t')

        if len(data) != 3:
            continue

        current_id, length_of_text, node_type  = data

        if text_id and current_id != text_id:
            print_line(text_id, question_length, answer_sum_length, answer_count)
            answer_count = 0
            answer_sum_length = 0
            question_length = 0

        text_id = current_id

        if node_type == 'question':
            question_length = int(length_of_text)
        else:
            answer_count += 1
            answer_sum_length += int(length_of_text)

    print_line(text_id, question_length, answer_sum_length, answer_count)



def print_line(text_id, question_length, answer_sum_length, answer_count):
    print text_id, '\t', question_length, '\t', \
          float(answer_sum_length) / max(answer_count, 1)

if __name__ == '__main__':
    reducer()
